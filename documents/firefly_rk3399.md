**Firefly rk3399**

| 参数         |                                                              |
| ------------ | ------------------------------------------------------------ |
| 主控芯片     | Rockchip RK3399 (28纳米HKMG制程）                            |
| 处理器       | 6核ARM® 64位处理器，主频高达1.8GHz基于big.LITTLE大小核架构，双核Cortex-A72(大核)+四核Cortex-A53(小核) |
| 图形处理器   | ARM Mali-T860 MP4 四核GPU支持OpenGL ES1.1/2.0/3.0/3.1, OpenVG1.1, OpenCL, DX11支持AFBC（帧缓冲压缩） |
| 视频处理器   | 支持4K VP9 and 4K 10bits H265/H264 视频解码，高达60fps1080P 多格式视频解码 (WMV, MPEG-1/2/4, VP8)1080P 视频编码，支持H.264，VP8格式视频后期处理器：反交错、去噪、边缘/细节/色彩优化 |
| 电源管理     | RK808 PMU芯片                                                |
| 内存         | 2GB/4GB双通道DDR3                                            |
| 存储器       | 16GB/32GB/128GB 高速eMMCMicroSD (TF) Card SlotPCIE M.2 NGFF ( B-KEY ) 接口 , 可用于扩展SSD |
| 硬件特性     |                                                              |
| 无线网络     | 板载WiFi模块（AP6356S）：2.4GHz/5GHz双频WiFi，支持802.11a/b/g/n/ac协议 、2x2 MIMO 技术Bluetooth 4.1（支持BLE） |
| 以太网       | 10/100/1000Mbps以太网 ( Realtek RTL8211E )                   |
| 显示         | 1 x HDMI 2.0 ( Type-A ), 支持4K@60帧输出1 x DP 1.2 (DisplayPort) , 支持4K@60帧输出1 x MIPI , 支持双通道2560x1600@60帧输出1 x eDP 1.3 ( 4 lanes with 10.8Gbps ) |
| 音频         | 1 x HDMI 或 1 x DP ( DispalyPort ) , 音频输出1 x 耳麦 , 用于音频输入输出1 x LINEOUT , 音频输出1 x SPEAKER , 喇叭输出 ( 1.5W 8Ω/2.5W 4Ω )1 x SPDIF 数字音频接口，用于音频输出1 x 麦克风，板载音频输入1 x I2S , 支持8通道 |
| 摄像头       | 2 x MIPI-CSI摄像头接口 ( 最高支持单13Mpixel 或 双8Mpixel )1 x DVP摄像头接口（最高支持5Mpixel） |
| PCIe         | 1 x Mini PCIe，用于LTE1 x PCIe 1.0, M.2接口                  |
| SIM          | 1 x SIM卡座，用于配合Mini PCIe扩展LTE模块                    |
| USB          | 4 x USB2.0 HOST，1 x USB3.0 HOST，1 x USB3.0 Type-C          |
| 红外         | 1 x 红外接收头，支持红外遥控功能                             |
| LED          | 1 x 电源状态LED (蓝色) ，1 x 自定义LED (绿色)                |
| RTC          | 支持RTC，板载备用电池接口                                    |
| 按键         | 1 x 复位键，1 x 电源键，1 x 升级键                           |
| 调试         | 1 x 调试串口，用于开发调试                                   |
| 预留接口     | 42pin 2.54mm排针：1 x I2S (8通道)、2 x ADC、2 x I2C、1 x SPI/UART、2 x GPIO、1 x LINEOUT、1 x SPEAKER |
| 电源         | DC12V - 2A ( 通过DC 5.5*2.1mm座 )                            |
| 系统/软件    |                                                              |
| 系统         | Android 7.1、Android 6.0、Ubuntu 16.04、u-boot               |
| 编程语言支持 | C、C++、Kotlin、Java、Shell、Pyhon等                         |
| 外观规格     |                                                              |
| PCB尺寸      | 124mm x 93mm （8层板设计，沉金工艺）                         |
| 重量         | 89克（带散热风扇：120克）                                    |



**Nanopc T4**

| **Hardware Spec**       |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| SoC                     | CPU: big.LITTLE，Dual-Core Cortex-A72(up to 2.0GHz) + Quad-Core Cortex-A53(up to 1.5GHz) GPU: Mali-T864 GPU，supports OpenGL ES1.1/2.0/3.0/3.1, OpenCL, DX11, and AFBC VPU: 4K VP9 and 4K 10bits H265/H264 60fps decoding, Dual VOP, etc |
| GPU:                    | Mali-T864 GPU，supports OpenGL® ES1.1/2.0/3.0/3.1, OpenCL™, DX11, and AFBC |
| VPU                     | 4K VP9 and 4K 10bits H265/H264 60fps decoding, Dual VOP, etc |
| DDR3 RAM:               | Dual-Channel 4GB LPDDR3                                      |
| Storage:                | 16GB eMMC 5.1 Flash                                          |
| Network:                | Native Gigabit Ethernet                                      |
| PMU Power Management:   | RK808-D PMIC, cooperated with independent DC/DC, enabling DVFS, sofeware power-down, RTC wake-up, system sleep mode |
| WiFi:                   | 802.11a/b/g/n/ac, Bluetooth 4.1, Wi-Fi and Bluetooth combo module, dual antenna interface |
| Antenna Interface:      | dual antenna interface                                       |
| Audio input/output Port | 3.5 mm audio jack / via HDMI,DVP Camera/MIPI-CSI (two camera interfaces),HDMI Type-A / LVDS / Parallel RGB-LCD / MIPI-DSI (four video output interfaces) |
| Bluetooth:              | Bluetooth 4.1,Wi-Fi and Bluetooth combo module, dual antenna interface |
| Microphone:             | onboard Microphone                                           |
| LCD Interface:          | one eDP 1.3（4 lane，10.8Gbps）, one 4-Lane MIPI-DSI         |
| HDMI:                   | HDMI 2.0a, supports 4K@60Hz，HDCP 1.4/2.2                    |
| GPIO:                   | 1 X 3V/1.8V I2C, up to 2 x 3V UART, 1 X 3V SPI, 1 x SPDIF_TX, up to 8 x 3V GPIOs 1 x 1.8V I2S, 3 x 1.8V GPIOs |
| Serial Debug Port:      | one Debug UART, 4 Pin 2.54mm header, 3V level, 1500000bps    |
| LED:                    | 1 x power LED and 1 x GPIO Controlled LED                    |
| RTC:                    | 2 Pin 1.27/1.25mm RTC battery input connector                |
| PCB Dimension:          | Ten Layer, 100 mm x 64 mm                                    |
| Power:                  | DC 12V/2A                                                    |
| Working temperature:    | -20℃ to 70℃                                                  |
| **Software Features**   |                                                              |
| Kernel version          | Linux-4.4-LTS U-boot-2014.10                                 |
| OS Support              | Android 8.1 Android 10 Lubuntu 16.04 (32-bit) FriendlyCore 18.04 (64-bit) FriendlyDesktop 18.04 (64-bit) FriendlyWrt 19.07.1 (64-bit) |