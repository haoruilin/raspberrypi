# 用qemu模拟器搭建arm运行环境

## 1. Ubuntu下搭建

主机：Ubuntu16.04 & 18.04

### 1.1 安装arm的交叉编译工具

打开终端输入安装命令：

`sudo apt-get install gcc-arm-linux-gnueabi `


### 1.2 编译Linux内核

#### 1.2.1 下载Linux内核

```bash
mkdir linux_kernel 
cd linux_kernel 
wget https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.19.tar.xz 
xz -d linux-4.19.tar.xz 
tar xvf linux-4.19.tar 
sudo apt install bison flex
```

#### 1.2.2 编译Linux内核

```bash
cd linux-4.19
# 生成开发板子的config文件
make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm vexpress_defconfig
# 编译
make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm
# 生成的内核镜像位于arch/arm/boot/zImage，后续qemu启动时需要使用该镜像。 
```

### 1.3 安装qemu模拟器

网上有直接很多现在qemu下载源码安装方法，但是测试发现其实直接用源安装也可以

#### 1.3.1 安装相关依赖

```bash
sudo apt install zlib1g-dev 
sudo apt install libglib2.0-0 
sudo apt install libglib2.0-dev 
```

#### 1.4.1 安装qemu

```bash
sudo apt install qemu 
```
#### 1.4.2 测试qemu和内核是否运行成功

```bash
qemu-system-arm -M vexpress-a9 -m 4096M -kernel /path_to_kernel_dir/arch/arm/boot/zImage -dtb  /path_to_kernel_dir/arch/arm/boot/dts/vexpress-v2p-ca9.dtb -nographic -append "console=ttyAMA0" 
```

上面是一条命令，其中：

`/path_to_kernel_dir/`是指内核编译目录

`-M vexpress-a9`是模拟vexpress-a9单板，可以使用`-M ?`餐胡来查询qemu支持的单板

`-m 4096M`指单板运行的物理内存

`-kernel /path_to_kernel_dir/arch/arm/boot/zImage`指定qemu单板运行内核镜像

`-nographic`指不使用图形化界面，只使用串口

`-append "console=ttyAMA0"`内核启动参数，这里告诉内核vexpress单板运行，串口设备是那个tty。

内核启动参数中的console=参数应该填上哪个tty，因为不同单板串口驱动类型不尽相同，创建的tty设备名当然也是不相同的。那vexpress单板的tty设备名填什么，可以从生成的.config文件CONFIG_CONSOLE宏找到。

如果搭建其它单板，需要注意内核启动参数的console=参数值，同样地，可从生成的.config文件中找到。

当输出显示
```bash
end Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(0,0) 
```
表示内核成功加载，但是没有根文件系统，小面制作根文件系统

### 1.5  制作根文件系统

Linux发行版的根文件系统很复杂，而我们这里用到的根文件系统很简单，我们要制作的根文件系统 =  busybox(包含基础的Linux命令)  + 运行库 + 几个字符设备。 根文件系统依赖于每个开发板支持的存储设备，可以放到Nor  Flash上，也可以放到SD卡，甚至外部磁盘上。最关键的一点是你要清楚知道开发板有什么存储设备。

#### 1.5.1 下载busybox

```bash
wget http://www.busybox.net/downloads/busybox-1.20.2.tar.bz2 
```

#### 1.5.2 编译安装busybox

```bash
tar xjvf busybox-1.27.2.tar.bz2 cd busybox-1.27.2 make defconfig make CROSS_COMPILE=arm-linux-gnueabi- make install CROSS_COMPILE=arm-linux-gnueabi- 
```

安装完成后，会在busybox目录下生成_install目录，该目录下的程序就是单板运行所需要的命令。

#### 1.5.3 形成根目录结构

先在Ubuntu主机环境下，形成目录结构，里面存放的文件和目录与单板上运行所需要的目录结构完全一样，然后再打包成镜像（在开发板看来就是SD卡），这个临时的目录结构称为根目录

创建rootfs目录（根目录），根文件系统内的文件全部放到这里

```bash
mkdir -p rootfs/{dev,etc/init.d,lib} 
```
拷贝busybox命令到根目录下

```bash
cp busybox-1.20.2/_install/* -r rootfs/ 
```

从工具链中拷贝运行库到lib目录下

```bash
sudo cp -P /usr/arm-linux-gnueabi/lib/* rootfs/lib/ 
```

创建4个tty端终设备

```bash
sudo mknod rootfs/dev/tty1 c 4 1 
sudo mknod rootfs/dev/tty2 c 4 2 
sudo mknod rootfs/dev/tty3 c 4 3 
sudo mknod rootfs/dev/tty4 c 4 4 
```

#### 1.5.4 制作根文件系统镜像

生成4096M大小的磁盘镜像

```bash
qemu-img create -f raw disk.img 512M 
```

格式化成ext4文件系统

```bash
mkfs -t ext4 ./disk.img 
```

将文件拷贝到镜像中

```bash
mkdir tmpfs 
sudo mount -o loop ./disk.img tmpfs/  
sudo cp -r rootfs/* tmpfs/ 
sudo umount tmpfs 
```

### 1.6 测试

#### 1.6.1 启动qemu模拟系统进行测试

完成上述所有步骤之后，就可以执行下面命令启动qemu来模拟vexpress开发板了：

```bash
qemu-system-arm -M vexpress-a9 -m 4096M -kernel /path_to_kernel_dir/arch/arm/boot/zImage -dtb  /path_to_kernel_dir/arch/arm/boot/dts/vexpress-v2p-ca9.dtb -nographic -append "root=/dev/mmcblk0  console=ttyAMA0" -sd a9rootfs.ext3 
```

执行命令内核启动打印完后，命令行出现命令提示符，说明基本安装完成，接下来执行在主机上交叉编译的代码

执行上面的问题发现arm系统中文件不能编辑，提示`read-only file system`，解决办法：

```bash
# 可以把 
-append "root=/dev/mmcblk0 console=ttyAMA0"  
# 改为 
-append "root=/dev/mmcblk0 rw console=ttyAMA0"  
```

#### 1.6.2 运行hello world

在用户目录创建hello.c源码`vim hello.c`：

```c
#inc#include <stdio.h>
int main()
{
	printf("hello world!");
	return 0;
}
```

用交叉编译工具编译程序

```bash
arm-linux-gnueabi-gcc-5 hello.c -o hello 
```

将交叉编译后生成的文件加载到arm的根目录

```bash
cp hello rootfs/ 
sudo mount -t ext3 a9rootfs.ext3 tmpfs -o loop 
sudo cp -r rootfs/* tmpfs 
sudo umount tmpfs 
```

再次启动qemu模拟系统测试程序

```bash
qemu-system-arm -M vexpress-a9 -m 4096M -kernel /path_to_kernel_dir/arch/arm/boot/zImage -dtb  /path_to_kernel_dir/arch/arm/boot/dts/vexpress-v2p-ca9.dtb -nographic -append "root=/dev/mmcblk0  console=ttyAMA0" -sd a9rootfs.ext3 
```

命令行中输入 ./hello
可以看到输出`hello world!`，说明gemu模拟的arm环境成功。
